Feature: Determine the type of triangle
  Triangles can be defined as different types. A triangle with equal sides is a equilateral triangle, with two equal
  sides is called a isosceles triangle and other triangles are called scalene triangles.

  Scenario: A triangle with all equal sides
    Given Input calculator is a available
    When 3 equals sides are inputted
    Then the triangle is qualified as 'equilateral'

  Scenario: A triangle with two equal sides
    Given Input calculator is a available
    When 2 equals sides are inputted
    Then the triangle is qualified as 'isosceles'

  Scenario: A triangle with no equal sides
    Given Input calculator is a available
    When 1 equals sides are inputted
    Then the triangle is qualified as 'scalene'
